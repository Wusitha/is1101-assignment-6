#include<stdio.h>

void printSeq(int n1, int n2, int limit)
{
	if(limit > 0)
	{
		printf("%d\n", n1+n2);
		printSeq(n2, n1+n2, limit-1);
	}
}

void fibonaccSeq(int n)
{
	int base = 0;
	printf("%d\n%d\n", base, base+1);
	printSeq(base , base+1, n-1);
}

int main()
{
	fibonaccSeq(10);
	return 0;
}
