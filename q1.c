#include<stdio.h>

void row(int n)
{
        printf("%d", n);
        if(n == 1)
        {
                printf("\n");
        }
        if(n > 1)
                row(n-1);
}

void drow(int n, int limit)
{
	if(limit>= n) 
	{
		row(n);
		drow(n+1, limit);
	}
}

void pattern(int n)
{
	if(n > 0)
		drow(1, n);
}

int main()
{
	pattern(4);
	return 0;
}
